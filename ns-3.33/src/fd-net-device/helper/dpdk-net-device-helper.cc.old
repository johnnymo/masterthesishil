/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2020 NITK Surathkal
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Harsh Patel <thadodaharsh10@gmail.com>
 *          Hrishikesh Hiraskar <hrishihiraskar@gmail.com>
 */

#include "dpdk-net-device-helper.h"

#include "ns3/dpdk-net-device.h"

#include <pthread.h>
#include <unistd.h>
#include <sys/signal.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("DpdkNetDeviceHelper");

DpdkNetDeviceHelper::DpdkNetDeviceHelper ()
  //Setting default parameters
  : m_lCoreList ("0,1"), 
    m_pmdLibrary ("librte_pmd_e1000.so"),
    m_dpdkDriver ("uio_pci_generic"),
    m_readLCore (1),
    m_memLibrary ("librte_mempool_ring.so"),
    m_mainLcore ("4")
{
  NS_LOG_FUNCTION (this);
  SetTypeId ("ns3::DpdkNetDevice");
}

void
DpdkNetDeviceHelper::SetLCoreList (std::string lCoreList)
{
  m_lCoreList = lCoreList;
}

void
DpdkNetDeviceHelper::SetPmdLibrary (std::string pmdLibrary)
{
  m_pmdLibrary = pmdLibrary;
}

void
DpdkNetDeviceHelper::SetDpdkDriver (std::string dpdkDriver)
{
  m_dpdkDriver = dpdkDriver;
}
void
DpdkNetDeviceHelper::SetLCore (uint16_t readLCore)
{
  m_readLCore = readLCore;
}
void
DpdkNetDeviceHelper::SetMemLibrary (std::string memLibrary)
{
  m_memLibrary = memLibrary;
}
void
DpdkNetDeviceHelper::SetmainLcore (std::string mainLcore)
{
  m_mainLcore = mainLcore;
}

void
DpdkNetDeviceHelper::InitDpdkEal(std::string dev0, std::string dev1)
{
  //signal (SIGINT, SignalHandler);
  //signal (SIGTERM, SignalHandler);

  NS_LOG_INFO ("Initialize DPDK EAL");
    // Set EAL arguments
  char **ealArgv = new char*[20];

  // arg[0] is program name (optional)
  ealArgv[0] = new char[20];
  strcpy (ealArgv[0], "");

  // Logical core usage
  ealArgv[1] = new char[20];
  strcpy (ealArgv[1], "-l");
  ealArgv[2] = new char[20];
  strcpy (ealArgv[2], m_lCoreList.c_str ());

  // Load library
  ealArgv[3] = new char[20];
  strcpy (ealArgv[3], "-d");
  ealArgv[4] = new char[80];
  strcpy (ealArgv[4], m_pmdLibrary.c_str ());


  // Use mempool ring library
  ealArgv[5] = new char[20];
  strcpy (ealArgv[5], "-d");
  ealArgv[6] = new char[80];
  strcpy (ealArgv[6], m_memLibrary.c_str ());

  // Set main core
/*
  ealArgv[7] = new char[20];
  strcpy (ealArgv[7], "--main-lcore");
  ealArgv[8] = new char[20];
  strcpy (ealArgv[8], m_mainLcore.c_str ());
*/
/*
  // Use Hugepages
  ealArgv[7] = new char[50];
  strcpy (ealArgv[7], "--in-memory");
*/
/*
  //Use memory legacy mode
  ealArgv[7] = new char[20];
  strcpy (ealArgv[7], "-n");
  ealArgv[8] = new char[50];
  strcpy (ealArgv[8], "5");
*/
  
  NS_LOG_INFO ("Binding device to DPDK");
  std::string command;
  command.append ("dpdk-devbind.py --force ");
  command.append ("--bind=");
  command.append (m_dpdkDriver.c_str ());
  command.append (" ");
  command.append (dev0.c_str ());
  command.append (" ");
  command.append (dev1.c_str ());
  printf ("Executing: %s\n", command.c_str ());
  if (system (command.c_str ()))
    {
      rte_exit (EXIT_FAILURE, "Execution failed - bye\n");
    }

  // wait for the device to bind to Dpdk
  sleep (5);  /* 5 seconds */

  int ret = rte_eal_init (7, ealArgv);
  if (ret < 0)
    {
      rte_exit (EXIT_FAILURE, "Invalid EAL arguments\n");
    }
    
}

Ptr<NetDevice>
DpdkNetDeviceHelper::InstallPriv (Ptr<Node> node) const
{
  NS_LOG_FUNCTION (this);
  Ptr<NetDevice> d = FdNetDeviceHelper::InstallPriv (node);
  Ptr<FdNetDevice> device = d->GetObject<FdNetDevice> ();


  Ptr<DpdkNetDevice> dpdkDevice = StaticCast<DpdkNetDevice> (device);
  dpdkDevice->SetDeviceName (m_deviceName);
  dpdkDevice->InitDpdk (m_dpdkDriver, m_readLCore);

  return device;
}


} // namespace ns3
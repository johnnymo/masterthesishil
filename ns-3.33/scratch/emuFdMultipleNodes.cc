//                 Network topology
//   +--------------+            +--------------+
//   | Raspberry Pi |            | Raspberry Pi |
//   |    Master    |            |    Slave     |
//   | IP:10.1.1.10 |            | IP:10.1.6.10 |
//   +--------------+            +--------------+
//           |                           |
//           |                           |
//  +--------|---------------------------|---------+
//  |    +--------+                  +--------+    |
//  |    |enp4s0f0|                  |enp4s0f3|    |
//  |-+--+--------+--+------------+--+--------+--+-|
//  | | FdNetDevice  |            |  FdNetDevice | |
//  | +--|--------|--+            +--|--------|--+ |
//  | |  |   n0   |  |            |  |   n1   |  | |
//  | +--|--------|--+            +--|--------|--+ |
//  | |   ptpDevice  |            |   ptpDevice  | |
//  | +--------------+            +--------------+ |
//  |    ptp0|                        ptp3|        |
//  |        |                            |        |
//  |       n2------------n3-------------n4        |
//  |            ptp1            ptp2              |
//  |                                              |
//  |                 ns3-simulation               |
//  +----------------------------------------------+
//  |                 Host computer                |
//  +----------------------------------------------+
//
//
// All nodes n2-n4 has two p2p devices installed.
//
// The simulation set up two nodes. Each node have an emuFdNetDevice 
// who are connected to a physical network interface card (NIC). 
// Each node has an csma device installed, and connected to an csma 
// channel. The FdNetDevice and the csmaDevice is bridged within the 
// nodes. 
//
// .pcap-files is written to output file, as well as routing tables
// and arp cache for every node to root folder for waf.
// 
// To run simulation go to the root repository and enter:
// sudo ./waf --run emuFdSwitchTopo 
// 
// To run with edited values (example):
// sudo ./waf --run "emuFdSwitchTopo --runTime=20 --int0=eth0 --int0=eth1"
//
// NB: All physical NICs used in the simulation must be in promiscuous 
//     mode.
//
// download ifconfig with: 
// sudo apt install net-tools
// Then:
// ifconfig enp5s0f0 promisc up
// ifconfig enp5s0f3 promisc up
// sudo dpdk-devbind.py --force --bind=igb 0000:05:00.0 0000:05:00.3
 


#include "ns3/abort.h"
#include "ns3/csma-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/fd-net-device-module.h"
#include "ns3/internet-apps-module.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/applications-module.h"
#include "ns3/bridge-module.h"
#include "ns3/point-to-point-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("EmuFdSwitchTopoMaster");

int
main (int argc, char *argv[])
{
    LogComponentEnable ("EmuFdSwitchTopoMaster", LOG_INFO);
    
    //Defining variables
    std::string ethInt0 ("enp4s0f0"); //name of first interface
    std::string ethInt1 ("enp4s0f3"); //name of second interface
    std::int32_t runTime (60); // How many seconds to run the simulation
    std::string csmaPcapString ("emuFdSwitchTopo-csma");
    std::string fd0PcapString ("emuFdSwitchTopo-enp5s0f0");
    std::string fd1PcapString ("emuFdSwitchTopo-enp5s0f3");
    std::string routesOutput ("routes-emuFdSwitchTopo.routes");

    //Allow user to override parameter defaults at run-time
    CommandLine cmd (__FILE__);
    cmd.AddValue ("int0", "First physical interface name", ethInt0);
    cmd.AddValue ("int1", "Second physical interface name", ethInt1);
    cmd.AddValue ("runTime", "How many seconds to run simulation", runTime);
    cmd.Parse (argc, argv);
    
    NS_LOG_INFO ("Switch topology EmuFdNetDevice between " << ethInt0 << " and " << ethInt1);

    //Setting realtime simulation and calculation of checksums
    GlobalValue::Bind ("SimulatorImplementationType", StringValue ("ns3::RealtimeSimulatorImpl"));
    GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));

    NS_LOG_DEBUG ("Adding fd nodes");
    //Create two nodes, one for each physical interface. e.g. one for each fd device
    NodeContainer nodes;
    nodes.Create (5);

    NS_LOG_DEBUG ("Adding CSMA-channel and installing fd nodes to the channel");
    //Creating csma channel and associate the devices to the channel
    PointToPointHelper ptp0, ptp1, ptp2, ptp3;
    NetDeviceContainer ptpDevices0, ptpDevices1, ptpDevices2, ptpDevices3;

    ptpDevices0 = ptp0.Install (nodes.Get(0),nodes.Get(2));
    ptpDevices1 = ptp1.Install (nodes.Get(2),nodes.Get(3));
    //ptp2.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (100)));
    ptpDevices2 = ptp2.Install (nodes.Get(3),nodes.Get(4));
    ptpDevices3 = ptp3.Install (nodes.Get(4),nodes.Get(1));
    

    NS_LOG_DEBUG ("Creating fdNetdevices towards physical interface");
    //Creating fdNetdevices and associate the physcial interface to it
    EmuFdNetDeviceHelper emu0, emu1;
    emu0.SetDeviceName (ethInt0);
    emu1.SetDeviceName (ethInt1);
    NetDeviceContainer devices0 = emu0.Install (nodes.Get(0));
    NetDeviceContainer devices1 = emu1.Install (nodes.Get(1));
    Ptr<NetDevice> device0 = devices0.Get (0);
    Ptr<NetDevice> device1 = devices1.Get (0);

    //Assign random mac-addresses to devices. Start from 00::01 and iterates.
    device0->SetAttribute ("Address", Mac48AddressValue (Mac48Address::Allocate ()));
    device1->SetAttribute ("Address", Mac48AddressValue (Mac48Address::Allocate ()));

    NS_LOG_DEBUG ("Installing internet stack to fd nodes");
    //installing the internet stack to the fdnodes
    InternetStackHelper stack;
    stack.Install (nodes);

    Ipv4AddressHelper address0, address1, address2, address3, address4, address5;
    address0.SetBase ("10.1.1.0", "255.255.255.0");
    address1.SetBase ("10.1.2.0", "255.255.255.0");
    address2.SetBase ("10.1.3.0", "255.255.255.0");
    address3.SetBase ("10.1.4.0", "255.255.255.0");
    address4.SetBase ("10.1.5.0", "255.255.255.0");
    address5.SetBase ("10.1.6.0", "255.255.255.0");
    
    Ipv4InterfaceContainer p2pInterfaces0, p2pInterfaces1, p2pInterfaces2, p2pInterfaces3, p2pInterfaces4, p2pInterfaces5;
    p2pInterfaces0 = address0.Assign (NetDeviceContainer(devices0));
    p2pInterfaces1 = address1.Assign (NetDeviceContainer(ptpDevices0));
    p2pInterfaces2 = address2.Assign (NetDeviceContainer(ptpDevices1));
    p2pInterfaces3 = address3.Assign (NetDeviceContainer(ptpDevices2));
    p2pInterfaces4 = address4.Assign (NetDeviceContainer(ptpDevices3));
    p2pInterfaces5 = address5.Assign (devices1);

    //Automatic set up of routing table by the use of routing algorithm based on OSPF https://www.nsnam.org/docs/models/html/routing-overview.html
    Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

    //store pcap files on every device
    emu0.EnablePcap (fd0PcapString, devices0, true);
    emu1.EnablePcap (fd1PcapString, devices1, true);

    //output routing protocol and arp cache of every node to routes-emuTest02.txt
    Ipv4GlobalRoutingHelper g;
    Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> (routesOutput, std::ios::out);
    g.PrintRoutingTableAllAt (Seconds (10), routingStream);
    g.PrintNeighborCacheAllAt (Seconds (10), routingStream);

    NS_LOG_INFO ("Running simulation for " << runTime << " seconds");
    Simulator::Stop (Seconds (runTime));
    Simulator::Run ();
    Simulator::Destroy ();

    //Logging output files to screen
    NS_LOG_INFO ("CSMA channel traffic stored in:                        " << csmaPcapString << "-X-Y.pcap");
    NS_LOG_INFO ("Incoming traffic for interface " << ethInt0 << " stored in:     " << fd0PcapString << "-X-Y.pcap");
    NS_LOG_INFO ("Incoming traffic for interface " << ethInt1 << " stored in:     " << fd1PcapString << "-X-Y.pcap");
    NS_LOG_INFO ("routing and forwarding tables for all nodes stored in: " << routesOutput);
    return 0;

}
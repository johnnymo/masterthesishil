// 
//                   Network topology
//     +--------------+            +--------------+
//     | Raspberry Pi |            | Raspberry Pi |
//     |    Master    |            |    Slave     |
//     | IP:10.1.1.10 |            | IP:10.1.1.11 |
//     +--------------+            +--------------+
//             |                           |
//             |                           |
//  +----------|---------------------------|------------+
//  |    +------------+              +------------+     |
//  |    |0000:04:00.0|              |0000:04:00.3|     |
//  |    |  enp4s0f0  |              |  enp4s0f3  |     |
//  |-+--+------------+--+-------+---+------------+--+--|
//  | |  dpdkNetDevice   |       |   dpdkNetDevice   |  |
//  | +----|--------|----+       +-----|--------|----+  |
//  | |    |   n0   |    |       |     |   n1   |    |  |
//  | +----|--------|----+       +-----|--------|----+  |
//  | |    csmaDevice    |       |     csmaDevice    |  |
//  | +------------------+       +-------------------+  |
//  |        |                             |            |
//  |        ===============================            |
//  |                    CSMA channel                   |
//  |                                                   |
//  |                   ns3-simulation                  |
//  +---------------------------------------------------+
//  |                   Host computer                   |
//  +---------------------------------------------------+
//
// My CPU layout from dpdk's usertools/cpu_layout.py:
/*
======================================================================
Core and Socket Information (as reported by '/sys/devices/system/cpu')
======================================================================

cores =  [0, 1, 2, 3, 4, 5]
sockets =  [0]

       Socket 0       
       --------       
Core 0 [0, 6]         
Core 1 [1, 7]         
Core 2 [2, 8]         
Core 3 [3, 9]         
Core 4 [4, 10]        
Core 5 [5, 11]
*/

#include "ns3/abort.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/fd-net-device-module.h"
#include "ns3/internet-apps-module.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/applications-module.h"
#include "ns3/traffic-control-module.h"
#include "ns3/csma-module.h"
#include "ns3/bridge-module.h"


using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("DpdkSwitchTopo");


int
main (int argc, char *argv[])
{
    LogComponentEnable ("DpdkSwitchTopo", LOG_INFO);
    
    //Set Default Realtime simulator to Hard Limit (1) instead of Best Effort (0). Default value is 0.1 seconds off from realtime and the program will terminate.
    //Config::SetDefault ("ns3::RealtimeSimulatorImpl::SynchronizationMode", EnumValue (1));
    //Uncomment line below to set other offsets from real clock scheduler to something other than 0.1 seconds
    //Config::SetDefault ("ns3::RealtimeSimulatorImpl::HardLimit", TimeValue (MicroSeconds (1)));
    NS_LOG_DEBUG ("Setting real time simulation");
    GlobalValue::Bind ("SimulatorImplementationType", StringValue ("ns3::RealtimeSimulatorImpl"));
    GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));

    std::string devName[2] = { "0000:05:00.0", "0000:05:00.3" };// Device enp5s0f0 and enp5s0f3 pci number. "lspci" to find
    //std::string dpdkDriver ("uio_pci_generic"); //generic driver
    //Virtual Function I/O (vfio) driver. IOMMU/device agnostic framework for exposing direct device access to userspace,in a secure, IOMMU protected environment
    //See https://www.kernel.org/doc/Documentation/vfio.txt
    std::string dpdkDriver ("vfio-pci"); //vfio-pci driver
    std::string pmdLibrary ("librte_net_e1000.so"); //I350 NIC should use librte_net_e1000 according to http://doc.dpdk.org/guides/nics/igb.html. Should also work for integrated NIC I219-LM
    std::string memLibrary ("librte_mempool_ring.so");
    std::string lCorelist ("4,5,10,11"); //set what logical cores you want to use in the simulation (int). See cpu cores with "lscpu | grep CPU" or "lstopo"

    //Ipv4Address localIp0 ("10.1.1.1"); // set IP-address of first node. Mostly for pingning to see how far nodes can reach through the simulation.
    //Ipv4Address localIp1 ("10.1.1.2"); // set IP-address of second node. 
    //std::string localMask ("255.255.255.0"); //netmask for LAN

    std::string pcapDpdk ("dpdk-physPort-Pcap");
    std::string pcapCsma ("dpdk-csma-Pcap");
    std::string routesOut ("dpdk-routesOutput.routes");

    //Create nodes
    NS_LOG_DEBUG ("Creating nodes");
    NodeContainer nodes;
    nodes.Create(2);

    //Create a Dpdk NetDevice using Dpdk NetDeviceHelper
    NS_LOG_DEBUG ("Creating DpdkNetDevice");
    DpdkNetDeviceHelper dpdk;

    dpdk.SetAttribute ("MaxTxBurst", UintegerValue (1));

    //Setting parameters for first dpdk device
    NS_LOG_DEBUG ("Setting Parameters for dpdk device");
    NS_LOG_DEBUG ("DeviceName: " << devName[0] << "\nDPDK driver set to: " << dpdkDriver << "\npmdLibrary set to: " << pmdLibrary << "\nlCorelist set to: " << lCorelist);
    dpdk.SetDeviceName (devName[0]);
    dpdk.SetDpdkDriver (dpdkDriver);
    dpdk.SetPmdLibrary (pmdLibrary);
    dpdk.SetMemLibrary (memLibrary);
    dpdk.SetLCoreList (lCorelist);
    dpdk.InitDpdkEal(2,devName);
    dpdk.SetLCore(5); //Set what core number this device shall use. It must be listed in the lCorelist otherwise the simulation won't run further.

    //Install first dpdk device into node 0 and add it to the device container. This will start the dpdk device.
    NS_LOG_DEBUG ("Installing DpdkNetDevice into node 0 and starting device");
    NetDeviceContainer devices = dpdk.Install (nodes.Get(0));

    NS_LOG_DEBUG("Setting devicename for next device to: " << devName[1]);
    dpdk.SetDeviceName (devName[1]);
    dpdk.SetLCore(11); //Set what core number this device shall use. It must be listed in the lCorelist otherwise the simulation will stop.

    //Install dpdk device into node 1. Also add the device into the devices container
    NS_LOG_DEBUG ("Installing DpdkNetDevice into node 1 and starting device");
    devices.Add(dpdk.Install (nodes.Get(1)));
    
    NS_LOG_DEBUG ("Setting MAC address to node 0's device");
    Ptr<NetDevice> device0 = devices.Get (0);
    //device0->SetAttribute ("Address", Mac48AddressValue (Mac48Address::Allocate ()));
    NS_LOG_DEBUG ("Setting MAC address to node 1's device");
    Ptr<NetDevice> device1 = devices.Get (1);
    //device1->SetAttribute ("Address", Mac48AddressValue (Mac48Address::Allocate ()));
 
    NS_LOG_DEBUG ("Installing IP-stack to all nodes");
    InternetStackHelper stack;
    stack.Install(nodes);

    NS_LOG_DEBUG ("Setting IP-address to both devices");
    Ipv4AddressHelper address;
    address.SetBase ("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer interface0 = address.Assign (device0);
    Ipv4InterfaceContainer interface1 = address.Assign (device1);

    NS_LOG_DEBUG ("Installing csma device to all nodes");
    CsmaHelper csma;
    NetDeviceContainer csmaDevices;
    csmaDevices = csma.Install(nodes);

    //Bridging between the physical interface and the virtual csma interface
    NS_LOG_DEBUG ("Bridging dpdk devices and csma devices in nodes");
    BridgeHelper fdBr0, fdBr1;
    fdBr0.Install (nodes.Get(0), NetDeviceContainer(device0, csmaDevices.Get(0)));
    fdBr1.Install (nodes.Get(1), NetDeviceContainer(device1, csmaDevices.Get(1)));

    NS_LOG_INFO("Writing pcap files from dpdk devices to prefix: " << pcapDpdk << "\nWriting pcap files from csma devices to prefix: " << pcapCsma);
    dpdk.EnablePcap ("dpdk-ports-Pcap", devices, true);
    csma.EnablePcap ("dpdk-csma-Pcap", csmaDevices, true);
    Ipv4GlobalRoutingHelper g;
    NS_LOG_INFO("Writing routing tables to prefix: " << routesOut);
    Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> (routesOut, std::ios::out);
    g.PrintRoutingTableAllAt (Seconds (20), routingStream);
    g.PrintNeighborCacheAllAt (Seconds (20), routingStream);

    NS_LOG_INFO ("Running the actual simulation");
    Simulator::Stop (Seconds (10));
    Simulator::Run ();
    Simulator::Destroy ();
}
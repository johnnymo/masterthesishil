// 
//                   Network topology
//     +--------------+            +--------------+
//     | Raspberry Pi |            | Raspberry Pi |
//     |    Master    |            |    Slave     |
//     | IP:10.1.1.10 |            | IP:10.1.2.10 |
//     +--------------+            +--------------+
//             |                           |
//             |.1                         |.1
//  +----------|---------------------------|------------+
//  |    +------------+              +------------+     |
//  |    |0000:05:00.0|              |0000:05:00.3|     |
//  |    |  enp5s0f0  |              |  enp5s0f3  |     |
//  |-+--+------------+--+-------+---+------------+--+--|
//  | |  dpdkNetDevice   |       |   dpdkNetDevice   |  |
//  | +----|--------|----+       +-----|--------|----+  |
//  | |    |   n0   |    |       |     |   n1   |    |  |
//  | +----|--------|----+       +-----|--------|----+  |
//  | |    csmaDevice    |       |     csmaDevice    |  |
//  | +------------------+       +-------------------+  |
//  |         ||.4.1                   .5.1||           |
//  |         ||                           ||           |
//  |         ||     WiFi:10.1.3.0/24      ||           |
//  |         ||             |             ||           |
//  |         ||             V             ||           |
//  |         || .4.2+--+ Y ~~~ Y +--+.5.2 ||           |
//  |         |======|n2|-+     +-|n3|======|           |
//  |               +--+.1     .2 +--+                  |
//  |                                                   |
//  |                                                   |
//  |                                                   |
//  |                                                   |
//  |              Simulated LTE Network                |
//  |                                                   |
//  |                   ns3-simulation                  |
//  +---------------------------------------------------+
//  |                   Host computer                   |
//  +---------------------------------------------------+ 
//
// My CPU layout from dpdk's usertools/cpu_layout.py:
/*
======================================================================
Core and Socket Information (as reported by '/sys/devices/system/cpu')
======================================================================

cores =  [0, 1, 2, 3, 4, 5]
sockets =  [0]

       Socket 0       
       --------       
Core 0 [0, 6]         
Core 1 [1, 7]         
Core 2 [2, 8]         
Core 3 [3, 9]         
Core 4 [4, 10]        
Core 5 [5, 11]
*/
//Based on wifi-simple-adhoc.cc in /examples/wireless/ for wifi implementation

#include "ns3/abort.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/fd-net-device-module.h"
#include "ns3/internet-apps-module.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/applications-module.h"
#include "ns3/traffic-control-module.h"
#include "ns3/csma-module.h"
#include "ns3/mobility-module.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-helper.h"
//#include "ns3/bridge-module.h"
//#include "ns3/lte-module.h"



using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("DpdkSwitchTopo");


int
main (int argc, char *argv[])
{
    LogComponentEnable ("DpdkSwitchTopo", LOG_INFO);
    
    //Set Default Realtime simulator to Hard Limit (1) instead of Best Effort (0). Default value is 0.1 seconds off from realtime and the program will terminate.
    //Config::SetDefault ("ns3::RealtimeSimulatorImpl::SynchronizationMode", EnumValue (1));
    //Uncomment line below to set other offsets from real clock scheduler to something other than 0.1 seconds
    //Config::SetDefault ("ns3::RealtimeSimulatorImpl::HardLimit", TimeValue (MicroSeconds (1)));
    NS_LOG_DEBUG ("Setting real time simulation");
    GlobalValue::Bind ("SimulatorImplementationType", StringValue ("ns3::RealtimeSimulatorImpl"));
    GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));

    std::string devName[2] = { "0000:05:00.0", "0000:05:00.3" };// Device enp5s0f0 and enp5s0f3 pci number. "lspci" to find
    //std::string dpdkDriver ("uio_pci_generic"); //generic driver
    //Virtual Function I/O (vfio) driver. IOMMU/device agnostic framework for exposing direct device access to userspace,in a secure, IOMMU protected environment
    //See https://www.kernel.org/doc/Documentation/vfio.txt
    std::string dpdkDriver ("vfio-pci"); //vfio-pci driver
    std::string pmdLibrary ("librte_net_e1000.so"); //I350 NIC should use librte_net_e1000 according to http://doc.dpdk.org/guides/nics/igb.html. Should also work for integrated NIC I219-LM
    std::string memLibrary ("librte_mempool_ring.so");
    std::string lCorelist ("4,5,10,11"); //set what logical cores you want to use in the simulation (int). See cpu cores with "lscpu | grep CPU" or "lstopo"

    std::string pcapDpdk ("dpdk-physPort-Pcap");
    std::string pcapCsma ("dpdk-csma-Pcap");
    std::string routesOut ("dpdk-Wifi.routes");

    //Wifi Parameters
    double rss = -80;  // -dBm

    //Create nodes
    NS_LOG_DEBUG ("Creating nodes");
    NodeContainer dpdkNodes;
    dpdkNodes.Create(2);
    NodeContainer wifiNodes;
    wifiNodes.Create(2);

    //Set some Wifi parameters
    WifiHelper wifi;
    wifi.SetStandard (WIFI_STANDARD_80211b);

    YansWifiPhyHelper wifiPhy;
    // This is one parameter that matters when using FixedRssLossModel
    // set it to zero; otherwise, gain will be added
    wifiPhy.Set ("RxGain", DoubleValue (0) );
    // ns-3 supports RadioTap and Prism tracing extensions for 802.11b
    wifiPhy.SetPcapDataLinkType (WifiPhyHelper::DLT_IEEE802_11_RADIO);

    YansWifiChannelHelper wifiChannel;
    wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
    // The below FixedRssLossModel will cause the rss to be fixed regardless
    // of the distance between the two stations, and the transmit power
    wifiChannel.AddPropagationLoss ("ns3::FixedRssLossModel","Rss",DoubleValue (rss));
    wifiPhy.SetChannel (wifiChannel.Create ());

    // Add a mac and disable rate control
    WifiMacHelper wifiMac;
    wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                "DataMode",StringValue (std::string ("DsssRate1Mbps")),
                                "ControlMode",StringValue (std::string ("DsssRate1Mbps")));

    // Set it to adhoc mode
    wifiMac.SetType ("ns3::AdhocWifiMac");
    NetDeviceContainer wifiDevices = wifi.Install (wifiPhy, wifiMac, wifiNodes);

    // Note that with FixedRssLossModel, the positions below are not
    // used for received signal strength.
    MobilityHelper mobility;
    Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
    positionAlloc->Add (Vector (0.0, 0.0, 0.0));
    positionAlloc->Add (Vector (5.0, 0.0, 0.0));
    mobility.SetPositionAllocator (positionAlloc);
    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    mobility.Install (wifiNodes);

    //Create a Dpdk NetDevice using Dpdk NetDeviceHelper
    NS_LOG_DEBUG ("Creating DpdkNetDevice");
    DpdkNetDeviceHelper dpdk;

    dpdk.SetAttribute ("MaxTxBurst", UintegerValue (1));

    //Setting parameters for first dpdk device
    NS_LOG_DEBUG ("Setting Parameters for dpdk device");
    NS_LOG_DEBUG ("DeviceName: " << devName[0] << "\nDPDK driver set to: " << dpdkDriver << "\npmdLibrary set to: " << pmdLibrary << "\nlCorelist set to: " << lCorelist);
    dpdk.SetDeviceName (devName[0]);
    dpdk.SetDpdkDriver (dpdkDriver);
    dpdk.SetPmdLibrary (pmdLibrary);
    dpdk.SetMemLibrary (memLibrary);
    dpdk.SetLCoreList (lCorelist);
    dpdk.InitDpdkEal(2, devName);
    dpdk.SetLCore(5); //Set what core number this device shall use. It must be listed in the lCorelist otherwise the simulation won't run further.

    //Install first dpdk device into node 0 and add it to the device container. This will start the dpdk device.
    NS_LOG_DEBUG ("Installing DpdkNetDevice into node 0 and starting device");
    NetDeviceContainer devices = dpdk.Install (dpdkNodes.Get(0));

    NS_LOG_DEBUG("Setting devicename for next device to: " << devName[1]);
    dpdk.SetDeviceName (devName[1]);
    dpdk.SetLCore(11); //Set what core number this device shall use. It must be listed in the lCorelist otherwise the simulation will stop.

    //Install dpdk device into node 1. Also add the device into the devices container
    NS_LOG_DEBUG ("Installing DpdkNetDevice into node 1 and starting device");
    devices.Add(dpdk.Install (dpdkNodes.Get(1)));
 
    NS_LOG_DEBUG ("Installing IP-stack to all nodes");
    InternetStackHelper stack;
    stack.Install (NodeContainer::GetGlobal ());

    NS_LOG_DEBUG ("Installing csma device to all nodes");
    CsmaHelper csma0, csma1;
    NetDeviceContainer csma0Devices, csma1Devices;
    csma0Devices = csma0.Install(NodeContainer(dpdkNodes.Get(0), wifiNodes.Get(0)));
    csma1Devices = csma1.Install(NodeContainer(dpdkNodes.Get(1), wifiNodes.Get(1)));

    NS_LOG_DEBUG ("Setting IP-address to all devices");
    NS_LOG_DEBUG ("Defining IP-networks");
    Ipv4AddressHelper addressDpdk0, addressDpdk1, addressWifi, addressCsma0, addressCsma1;
    addressDpdk0.SetBase    ("10.1.1.0", "255.255.255.0");
    addressDpdk1.SetBase    ("10.1.2.0", "255.255.255.0");
    addressWifi.SetBase     ("10.1.3.0", "255.255.255.0");
    addressCsma0.SetBase    ("10.1.4.0", "255.255.255.0");
    addressCsma1.SetBase    ("10.1.5.0", "255.255.255.0");

    NS_LOG_DEBUG ("Assigning IP-addresses to al interfaces");
    Ipv4InterfaceContainer interface0 = addressDpdk0.Assign (devices.Get (0));
    Ipv4InterfaceContainer interface1 = addressDpdk1.Assign (devices.Get (1));
    Ipv4InterfaceContainer interface2 = addressCsma0.Assign (csma0Devices);
    Ipv4InterfaceContainer interface3 = addressCsma1.Assign (csma1Devices);
    Ipv4InterfaceContainer interface4 = addressWifi.Assign (wifiDevices);

    Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

    NS_LOG_INFO("Writing pcap files from dpdk devices to prefix: " << pcapDpdk << "\nWriting pcap files from csma devices to prefix: " << pcapCsma);
    dpdk.EnablePcap ("dpdk-ports-Pcap", devices, true);
    csma0.EnablePcap ("dpdk-csma0", csma0Devices.Get(0), true);
    csma1.EnablePcap ("dpdk-csma1", csma1Devices.Get(0), true);
    wifiPhy.EnablePcap ("wifi-adhoc", wifiDevices, true);
    Ipv4GlobalRoutingHelper g;
    NS_LOG_INFO("Writing routing tables to prefix: " << routesOut);
    Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> (routesOut, std::ios::out);
    g.PrintRoutingTableAllAt (Seconds (5), routingStream);
    g.PrintNeighborCacheAllAt (Seconds (5), routingStream);

    NS_LOG_INFO ("Running the actual simulation");
    Simulator::Stop (Seconds (10));
    Simulator::Run ();
    Simulator::Destroy ();
}
// 
//                   Network topology
//      .10      .1       .1  .2         .2  .1          .1      .10 
//  +---------+   +--------+   +----------+   +-----------+   +--------+
//  |RpiMaster|===|dpdkN(0)|===|  Node(0) |===|  dpdkN(1) |===|RpiSlave|
//  +---------+   +--------+   +----------+   +-----------+   +--------+
//    10.1.1.0/24     | 10.1.3.0/25 | 10.1.3.128/25 |    10.1.2.0/24      
//
// My CPU layout from dpdk's usertools/cpu_layout.py:
/*
======================================================================
Core and Socket Information (as reported by '/sys/devices/system/cpu')
======================================================================

cores =  [0, 1, 2, 3, 4, 5]
sockets =  [0]

       Socket 0       
       --------       
Core 0 [0, 6]         
Core 1 [1, 7]         
Core 2 [2, 8]         
Core 3 [3, 9]         
Core 4 [4, 10]        
Core 5 [5, 11]
*/

#include "ns3/abort.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/fd-net-device-module.h"
#include "ns3/internet-apps-module.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/applications-module.h"
#include "ns3/traffic-control-module.h"
#include "ns3/csma-module.h"



using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("DpdkSwitchTopo");


int
main (int argc, char *argv[])
{
    LogComponentEnable ("DpdkSwitchTopo", LOG_INFO);
    
    //Set Default Realtime simulator to Hard Limit (1) instead of Best Effort (0). Default value is 0.1 seconds off from realtime and the program will terminate.
    //Config::SetDefault ("ns3::RealtimeSimulatorImpl::SynchronizationMode", EnumValue (1));
    //Uncomment line below to set other offsets from real clock scheduler to something other than 0.1 seconds
    //Config::SetDefault ("ns3::RealtimeSimulatorImpl::HardLimit", TimeValue (MicroSeconds (1)));
    NS_LOG_DEBUG ("Setting real time simulation");
    GlobalValue::Bind ("SimulatorImplementationType", StringValue ("ns3::RealtimeSimulatorImpl"));
    GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));

    std::string devName[2] = { "0000:05:00.0", "0000:05:00.3" };// Device enp5s0f0 and enp5s0f3 pci number. "lspci" to find
    //std::string dpdkDriver ("uio_pci_generic"); //generic driver
    //Virtual Function I/O (vfio) driver. IOMMU/device agnostic framework for exposing direct device access to userspace,in a secure, IOMMU protected environment
    //See https://www.kernel.org/doc/Documentation/vfio.txt
    std::string dpdkDriver ("vfio-pci"); //vfio-pci driver
    std::string pmdLibrary ("librte_net_e1000.so"); //I350 NIC should use librte_net_e1000 according to http://doc.dpdk.org/guides/nics/igb.html. Should also work for integrated NIC I219-LM
    std::string memLibrary ("librte_mempool_ring.so");
    std::string lCorelist ("4,5,10,11"); //set what logical cores you want to use in the simulation (int). See cpu cores with "lscpu | grep CPU" or "lstopo"

    std::string pcapDpdk ("dpdk-physPort-Pcap");
    std::string pcapCsma ("dpdk-csma-Pcap");
    std::string routesOut ("dpdk-OneNodeBetween.routes");

    //Create nodes
    NS_LOG_DEBUG ("Creating nodes");
    NodeContainer dpdkNodes;
    dpdkNodes.Create(2);
    NodeContainer node;
    node.Create(1);

    //Create a Dpdk NetDevice using Dpdk NetDeviceHelper
    NS_LOG_DEBUG ("Creating DpdkNetDevice");
    DpdkNetDeviceHelper dpdk;

    //Setting MaxTx burst to 1 to give lower latency and no packet batching
    dpdk.SetAttribute ("MaxTxBurst", UintegerValue (1));

    //Setting parameters for first dpdk device
    NS_LOG_DEBUG ("Setting Parameters for dpdk device");
    NS_LOG_DEBUG ("DeviceName: " << devName[0] << "\nDPDK driver set to: " << dpdkDriver << "\npmdLibrary set to: " << pmdLibrary << "\nlCorelist set to: " << lCorelist);
    dpdk.SetDeviceName (devName[0]);
    dpdk.SetDpdkDriver (dpdkDriver);
    dpdk.SetPmdLibrary (pmdLibrary);
    dpdk.SetMemLibrary (memLibrary);
    dpdk.SetLCoreList (lCorelist);
    dpdk.InitDpdkEal(2, devName);
    //Setting what core number this device shall use. It must be listed in the lCorelist otherwise the simulation will halt.
    dpdk.SetLCore(5); 

    //Install first dpdk device into node 0 and add it to the device container. This will start the dpdk device.
    NS_LOG_DEBUG ("Installing DpdkNetDevice into node 0 and starting device");
    NetDeviceContainer devices = dpdk.Install (dpdkNodes.Get(0));

    NS_LOG_DEBUG("Setting devicename for next device to: " << devName[1]);
    dpdk.SetDeviceName (devName[1]);
    //Setting what core number this device shall use. It must be listed in the lCorelist otherwise the simulation will halt.
    dpdk.SetLCore(11); 

    //Install dpdk device into node 1. Also add the device into the devices container
    NS_LOG_DEBUG ("Installing DpdkNetDevice into node 1 and starting device");
    devices.Add(dpdk.Install (dpdkNodes.Get(1)));
    
    NS_LOG_DEBUG ("Setting MAC address to node 0's device");
    Ptr<NetDevice> device0 = devices.Get (0);
    NS_LOG_DEBUG ("Setting MAC address to node 1's device");
    Ptr<NetDevice> device1 = devices.Get (1);
 
    NS_LOG_DEBUG ("Installing IP-stack to all nodes");
    InternetStackHelper stack;
    stack.Install(dpdkNodes);
    stack.Install(node);

    NS_LOG_DEBUG ("Installing csma device to all nodes");
    CsmaHelper csma0, csma1;
    NetDeviceContainer csma0Devices, csma1Devices;
    csma0Devices = csma0.Install(NodeContainer(dpdkNodes.Get(0), node.Get(0)));
    csma1Devices = csma1.Install(NodeContainer(dpdkNodes.Get(1), node.Get(0)));

    NS_LOG_DEBUG ("Setting IP-address to all devices");
    Ipv4AddressHelper addressDpdk0, addressDpdk1, addressMesh0, addressMesh1;
    addressDpdk0.SetBase ("10.1.1.0", "255.255.255.0");
    addressMesh0.SetBase ("10.1.3.0", "255.255.255.128");
    addressMesh1.SetBase ("10.1.3.128", "255.255.255.128");
    addressDpdk1.SetBase ("10.1.2.0", "255.255.255.0");
    Ipv4InterfaceContainer interface0 = addressDpdk0.Assign (device0);
    Ipv4InterfaceContainer interface1 = addressMesh0.Assign (csma0Devices);
    Ipv4InterfaceContainer interface2 = addressMesh1.Assign (csma1Devices);
    Ipv4InterfaceContainer interface3 = addressDpdk1.Assign (device1);

    //Automatic set up of routing table by the use of routing algorithm based on OSPF https://www.nsnam.org/docs/models/html/routing-overview.html
    Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

    NS_LOG_INFO("Writing pcap files from dpdk devices to prefix: " << pcapDpdk << "\nWriting pcap files from csma devices to prefix: " << pcapCsma);
    dpdk.EnablePcap (pcapDpdk, devices, true);
    csma0.EnablePcap (pcapCsma, csma0Devices.Get(1), true);
    csma1.EnablePcap (pcapCsma, csma1Devices.Get(1), true);
    //Logging routing tables and cache of all nodes every 20 seconds
    NS_LOG_INFO("Writing routing tables to prefix: " << routesOut);
    Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> (routesOut, std::ios::out);
    Ipv4GlobalRoutingHelper g;
    g.PrintRoutingTableAllAt (Seconds (20), routingStream);
    g.PrintNeighborCacheAllAt (Seconds (20), routingStream);

    NS_LOG_INFO ("Running the actual simulation");
    Simulator::Stop (Seconds (10));
    Simulator::Run ();
    Simulator::Destroy ();
}
//                 Network topology
//   +--------------+            +--------------+
//   | Raspberry Pi |            | Raspberry Pi |
//   |    Master    |            |    Slave     |
//   | IP:10.1.1.10 |            | IP:10.1.1.11 |
//   +--------------+            +--------------+
//           |                           |
//           |                           |
//  +--------|---------------------------|----------+
//  |    +--------+                  +--------+     |
//  |    |enp4s0f0|                  |enp4s0f3|     |
//  |-+-++--------+-+---------------++--------+-+-+-|
//  | | |FdNetDevice|===============|FdNetDevice| | | 
//  | | +-----------+      br       +-----------+ | |
//  | |                    n0                     | |
//  | +-------------------------------------------+ |
//  |                                               |
//  |                 ns3-simulation                |
//  +-----------------------------------------------+
//  |                 Host computer                 |
//  +-----------------------------------------------+
//
// The simulation set up two nodes. Each node have an emuFdNetDevice 
// who are connected to a physical network interface card (NIC). 
// Each node has an csma device installed, and connected to an csma 
// channel. The FdNetDevice and the csmaDevice is bridged within the 
// nodes. 
//
// .pcap-files is written to output file, as well as routing tables
// and arp cache for every node to root folder for waf.
// 
// To run simulation go to the root repository and enter:
// sudo ./waf --run emuFdSwitchTopo 
// 
// To run with edited values (example):
// sudo ./waf --run "emuFdSwitchTopo --runTime=20 --int0=eth0 --int0=eth1"
//
// NB: All physical NICs used in the simulation must be in promiscuous 
//     mode.
// download ifconfig with: sudo apt install net-tools
// Then:
// ifconfig enp4s0f0 promisc up
// ifconfig enp4s0f3 promisc up
// sudo dpdk-devbind.py --force --bind=igb 0000:04:00.0
 


#include "ns3/abort.h"
#include "ns3/csma-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/fd-net-device-module.h"
#include "ns3/internet-apps-module.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/applications-module.h"
#include "ns3/bridge-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("EmuFdOneNode");

int
main (int argc, char *argv[])
{
    LogComponentEnable ("EmuFdOneNode", LOG_INFO);
    
    //Defining variables
    std::string ethInt0 ("enp5s0f0"); //name of first interface
    std::string ethInt1 ("enp5s0f3"); //name of second interface
    std::int32_t runTime (10); // How many seconds to run the simulation
    std::string csmaPcapString ("emuFdOneNode-csma");
    std::string fd0PcapString ("emuFdOneNode-enp5s0f0");
    std::string fd1PcapString ("emuFdOneNode-enp5s0f3");
    std::string routesOutput ("routes-emuFdOneNode.routes");
    bool pcap = false;
    bool routeTable = false;

    //Allow user to override parameter defaults at run-time
    CommandLine cmd (__FILE__);
    cmd.AddValue ("int0", "First physical interface name", ethInt0);
    cmd.AddValue ("int1", "Second physical interface name", ethInt1);
    cmd.AddValue ("runTime", "How many seconds to run simulation", runTime);
    cmd.AddValue ("pcap", "Do you want output pcap files", pcap);
    cmd.AddValue ("routeTable", "Do you want routing table output from nodes", routeTable);
    cmd.Parse (argc, argv);
    
    NS_LOG_INFO ("Switch topology EmuFdNetDevice between " << ethInt0 << " and " << ethInt1 << " with one node");

    //Setting realtime simulation and calculation of checksums
    GlobalValue::Bind ("SimulatorImplementationType", StringValue ("ns3::RealtimeSimulatorImpl"));
    GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));

    NS_LOG_DEBUG ("Adding fd nodes");
    //Create two nodes, one for each physical interface. e.g. one for each fd device
    NodeContainer node;
    node.Create (1);

    NS_LOG_DEBUG ("Creating fdNetdevices towards physical interface");
    //Creating fdNetdevices and associate the physcial interface to it
    EmuFdNetDeviceHelper emu0, emu1;
    emu0.SetDeviceName (ethInt0);
    emu1.SetDeviceName (ethInt1);
    NetDeviceContainer devices = emu0.Install (node.Get(0));
    devices.Add(emu1.Install (node.Get(0)));

    NS_LOG_DEBUG ("setting up bridges");
    //Bridging between the physical interface and the virtual csma interface
    BridgeHelper br;
    br.Install (node.Get(0), NetDeviceContainer(devices.Get (0), devices.Get (1)));

    NS_LOG_DEBUG ("Installing internet stack to fd nodes");
    //installing the internet stack to the fdnodes
    InternetStackHelper stack;
    stack.Install (node);

    //store pcap files on every device
    if (pcap){
        emu0.EnablePcap (fd0PcapString, devices.Get (0), true);
        emu1.EnablePcap (fd1PcapString, devices.Get (1), true);
    }

    //output routing protocol and arp cache of every node to routes-emuTest02.txt
    if (routeTable){
        Ipv4GlobalRoutingHelper g;
        Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> (routesOutput, std::ios::out);
        g.PrintRoutingTableAllAt (Seconds (10), routingStream);
        g.PrintNeighborCacheAllAt (Seconds (10), routingStream);
    }

    NS_LOG_INFO ("Running simulation for " << runTime << " seconds");
    Simulator::Stop (Seconds (runTime));
    Simulator::Run ();
    Simulator::Destroy ();

    //Logging output files to screen
    if (pcap){
        NS_LOG_INFO ("CSMA channel traffic stored in:                        " << csmaPcapString << "-X-Y.pcap");
        NS_LOG_INFO ("Incoming traffic for interface " << ethInt0 << " stored in:     " << fd0PcapString << "-X-Y.pcap");
        NS_LOG_INFO ("Incoming traffic for interface " << ethInt1 << " stored in:     " << fd1PcapString << "-X-Y.pcap");
    }
    if (routeTable){
        NS_LOG_INFO ("routing and forwarding tables for all nodes stored in: " << routesOutput);
    }
    return 0;
}
/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// Network topology
//
// Packets sent to the device "tap0" on the Linux host will be sent to the
// tap bridge on node zero and then emitted onto the ns-3 simulated CSMA
// network.  ARP will be used on the CSMA network to resolve MAC addresses.
// Packets destined for the CSMA device on node zero will be sent to the
// device "tap0" on the linux Host. Same for tap 1.                                                     
//                                                                   
//  +-----------+                           +-----------+                                           
//  |  external |                           |  external |                                              
//  |raspberryPi|                           |raspberryPi|                                                   
//  |_____0_____|                           |_____1_____|                                                    
//  |   eth0    |                           |   eth0    |                                                                                  
//  +-----------+                           +-----------+                                                       
//       |                                        |
//  +-----------+                           +-----------+
//  | Interface |                           | Interface |
//  |  Ubuntu   |                           |  Ubuntu   |
//  |"enp4s0f0" |                           |"enp4s0f3" |
//  +-----------+                           +-----------+                                               
//       |                                       |
//  +----------+                            +----------+
//  |  Bridge  |                            |  Bridge  |
//  |  "br0"   |                            |  "br1"   |
//  +----------+                            +----------+                                              
//       |                                       |
//  +----------+                            +----------+
//  |Tap-Device|                            |Tap-Device|
//  |  "tap0"  |                            |  "tap1"  |
//  +----------+                            +----------+                                                       
//       |           n0            n1            |
//       |       +--------+    +--------+        |
//       +-------|  tap   |    |  tap   |--------+
//               | bridge |    | bridge |
//               +--------+    +--------+    
//               |  CSMA  |    |  CSMA  |
//               +--------+    +--------+
//                   |             |    
//                   ===============
//                    CSMA LAN 10.1.1
//
// The CSMA device on node one and zero is a ghost node which means it doesn't need an IP-address
//
// To configure your linux computer(Done in Ubuntu20.04):
/*
sudo tunctl -t tap0
sudo tunctl -t tap1
sudo brctl addbr br0
sudo brctl addbr br1
sudo brctl addif br0 tap0 enp5s0f0 
sudo brctl addif br1 tap1 enp5s0f3 
sudo ifconfig br0 promisc up
sudo ifconfig br1 promisc up
sudo ifconfig tap0 up
sudo ifconfig tap1 up
*/
//
// Some simple things to do:
//
// 1) Ping one of the raspberry pis through the simulated switch topology
//
// To run simulation: ./waf --run "tapSwitch --pcap --runTime=10" 
// On raspberry Pis:  ping 10.1.1.X
//
#include <iostream>
#include <fstream>

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/wifi-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/tap-bridge-module.h"
#include "ns3/data-collector.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("TapSwitch");

int main (int argc, char *argv[])
{
  LogComponentEnable ("TapSwitch", LOG_INFO);

  std::string mode = "UseBridge";
  std::string tapName01 = "tap0";
  std::string tapName02 = "tap1";
  std::string experiment = "tapSwitch";
  std::string strategy = "throughput";
  std::string input = "data";
  std::int32_t runTime (10); // How many seconds to run the simulation
  bool pcap = false;

  CommandLine cmd (__FILE__);
  cmd.AddValue ("runTime", "How many seconds to run simulation", runTime);
  cmd.AddValue ("pcap", "Do you want output pcap files", pcap);
  cmd.Parse (argc, argv);

  GlobalValue::Bind ("SimulatorImplementationType", StringValue ("ns3::RealtimeSimulatorImpl"));
  GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));

  //Create 2 nodes
  NodeContainer nodes;
  nodes.Create (2);

  CsmaHelper csma;
  //Install csma device to every
  NetDeviceContainer devices = csma.Install (nodes);
  
  //Install the ghost nodes for traffic tapping
  TapBridgeHelper tapBridge;
  tapBridge.SetAttribute ("Mode", StringValue (mode));
  tapBridge.SetAttribute ("DeviceName", StringValue (tapName01));
  tapBridge.Install (nodes.Get (0), devices.Get (0));
  tapBridge.SetAttribute ("DeviceName", StringValue (tapName02));
  tapBridge.Install (nodes.Get (1), devices.Get (1));
  
  if (pcap) {
    csma.EnablePcapAll ("tap-csma", true);
  }

  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

  NS_LOG_INFO ("Running simulation for " << runTime << " seconds");
  Simulator::Stop (Seconds (runTime));
  Simulator::Run ();
  Simulator::Destroy ();
}

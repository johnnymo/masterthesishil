// 
//                   Network topology
//     +--------------+            +--------------+
//     | Raspberry Pi |            | Raspberry Pi |
//     |    Master    |            |    Slave     |
//     | IP:10.1.1.10 |            | IP:10.1.1.11 |
//     +--------------+            +--------------+
//             |                           |
//             |                           |
//  +----------|---------------------------|-----------+
//  |    +------------+             +------------+     |
//  |    |0000:05:00.0|             |0000:05:00.3|     |
//  |    |  enp5s0f0  |             |  enp5s0f3  |     |
//  |-+-++------------++-----------++------------++--+-|
//  | | |dpdkNetDevice |===========|dpdkNetDevice |  | |
//  | | +--------------+    br     +--------------+  | |
//  | |                                              | |
//  | |                                              | |
//  | |                     n0                       | |
//  | +----------------------------------------------+ |
//  |                                                  |
//  |                   ns3-simulation                 |
//  +--------------------------------------------------+
//  |                   Host computer                  |
//  +--------------------------------------------------+
//
// My CPU layout from dpdk's usertools/cpu_layout.py:
/*
======================================================================
Core and Socket Information (as reported by '/sys/devices/system/cpu')
======================================================================

cores =  [0, 1, 2, 3, 4, 5]
sockets =  [0]

       Socket 0       
       --------       
Core 0 [0, 6]         
Core 1 [1, 7]         
Core 2 [2, 8]         
Core 3 [3, 9]         
Core 4 [4, 10]        
Core 5 [5, 11]
*/

#include "ns3/abort.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/fd-net-device-module.h"
#include "ns3/internet-apps-module.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/applications-module.h"
#include "ns3/traffic-control-module.h"
#include "ns3/csma-module.h"
#include "ns3/bridge-module.h"


using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("DpdkOneNode");

int
main (int argc, char *argv[])
{
    LogComponentEnable ("DpdkOneNode", LOG_INFO);

    //Commandline input for runTime
    std::int32_t runTime (10); // How many seconds to run the simulation
    std::uint16_t interf (2); // How many interfaces included
    bool pcap = false;
    bool routeTable = false;

    //Setting Realtime simulator
    NS_LOG_DEBUG ("Setting real time simulation");
    GlobalValue::Bind ("SimulatorImplementationType", StringValue ("ns3::RealtimeSimulatorImpl"));
    GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));
    
    //Setting DPDK variables 
    std::string dev[interf] = { "0000:05:00.0", "0000:05:00.3" };
    //std::string deviceName0 ("0000:05:00.0"); // Device enp4s0f0 pci number "lspci" to find
    //std::string deviceName1 ("0000:05:00.3"); // Device enp4s0f3 pci number "lspci" to find
    std::string dpdkDriver ("vfio-pci"); //vfio-pci driver
    std::string pmdLibrary ("librte_net_e1000.so"); //I350 NIC should use librte_net_e1000 according to http://doc.dpdk.org/guides/nics/igb.html. Should also work for integrated NIC I219-LM
    std::string memLibrary ("librte_mempool_ring.so");
    std::string lCorelist ("4,5,10,11"); //set what logical cores you want to use in the simulation (int). See cpu cores with "lscpu | grep CPU" or "lstopo"
    int firstLCore = 5; //Set what core number first device shall use. It must be listed in the lCorelist otherwise the simulation won't run.
    int secLCore = 11; //Set what core number second device shall use. It must be listed in the lCorelist otherwise the simulation won't run.

    //Output Filenames
    std::string dpdkPcapString ("dpdkOneNode-NetDev");
    std::string routesOut ("dpdk-routesOutput.routes");

    CommandLine cmd (__FILE__);
    cmd.AddValue ("runTime", "How many seconds to run simulation", runTime);

    cmd.AddValue ("pcap", "Do you want output pcap files", pcap);
    cmd.AddValue ("routeTable", "Do you want routing table output from nodes", routeTable);
    cmd.Parse (argc, argv);

    //Create nodes
    NS_LOG_DEBUG ("Creating nodes");
    NodeContainer nodes;
    nodes.Create(1);

    //Create a Dpdk NetDevice using Dpdk NetDeviceHelper
    NS_LOG_DEBUG ("Creating DpdkNetDevice");
    DpdkNetDeviceHelper dpdk;

    //Setting Max packet-burst in tx
    dpdk.SetAttribute ("MaxTxBurst", UintegerValue (1));
    
    //Setting parameters for first dpdk device
    NS_LOG_DEBUG ("Setting Parameters for dpdk device");
    NS_LOG_DEBUG ("DeviceName: " << dev[0] << "\nDPDK driver set to: " << dpdkDriver << "\npmdLibrary set to: " << pmdLibrary << "\nlCorelist set to: " << lCorelist);
    dpdk.SetDpdkDriver (dpdkDriver);
    dpdk.SetPmdLibrary (pmdLibrary);
    dpdk.SetMemLibrary (memLibrary);
    dpdk.SetLCoreList (lCorelist);
    dpdk.InitDpdkEal(interf, dev);
    dpdk.SetDeviceName (dev[0]);
    dpdk.SetLCore(firstLCore); 

    //Install first dpdk device into node 0 and add it to the device container. This will start the dpdk device.
    NS_LOG_DEBUG ("Installing DpdkNetDevice into node 0 and starting device");
    NetDeviceContainer devices = dpdk.Install (nodes.Get(0));

    //NS_LOG_DEBUG("Setting devicename for next device to: " << dev[1]);
    dpdk.SetDeviceName (dev[1]);
    dpdk.SetLCore(secLCore); 

    //Install dpdk device into node 1. Also add the device into the devices container
    NS_LOG_DEBUG ("Installing DpdkNetDevice into node 1 and starting device");
    devices.Add(dpdk.Install (nodes.Get(0)));
    
    NS_LOG_DEBUG ("Installing IP-stack to all nodes");
    InternetStackHelper stack;
    stack.Install(nodes);

    //Bridging between the physical interfaces with an ns3 bridge
    NS_LOG_DEBUG ("Bridging dpdk devices in node");
    BridgeHelper br;
    br.Install (nodes.Get(0), NetDeviceContainer(devices.Get (0), devices.Get (1)));

    if (pcap) {
        //Write Pcap files from the dpdk-devices 
        dpdk.EnablePcap (dpdkPcapString, devices.Get(0), true);
        dpdk.EnablePcap (dpdkPcapString, devices.Get(1), true);
    }
    if (routeTable) {
        //Write out routing tables and arp cache every 20 seconds
        Ipv4GlobalRoutingHelper g;
        Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> (routesOut, std::ios::out);
        g.PrintRoutingTableAllAt (Seconds (20), routingStream);
        g.PrintNeighborCacheAllAt (Seconds (20), routingStream);
    }

    NS_LOG_INFO ("Running the actual simulation for " << runTime << " seconds");
    Simulator::Stop (Seconds (runTime));
    Simulator::Run ();
    Simulator::Destroy ();

    //Logging output files to screen
    if (pcap) {
        NS_LOG_INFO ("Incoming traffic for interfaces stored in:             " << dpdkPcapString << "-X-Y.pcap");
    }
    if (routeTable) {
        NS_LOG_INFO ("routing and forwarding tables for all nodes stored in: " << routesOut);
    }
}

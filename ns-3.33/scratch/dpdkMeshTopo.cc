// 
//                   Network topology
//      Net:10.1.1.0/24             Net:10.1.2.0/24
//     +--------------+            +--------------+
//     | Raspberry Pi |            | Raspberry Pi |
//     |    Master    |            |    Slave     |
//     | IP:10.1.1.10 |            | IP:10.1.2.10 |
//     +--------------+            +--------------+
//             |                           |
//             |.1                         |.1
//  +----------|---------------------------|------------+
//  |    +------------+              +------------+     |
//  |    |0000:05:00.0|              |0000:05:00.3|     |
//  |    |  enp5s0f0  |              |  enp5s0f3  |     |
//  |-+--+------------+--+-------+---+------------+--+--|
//  | |  dpdkNetDevice   |       |   dpdkNetDevice   |  |
//  | +----|--------|----+       +-----|--------|----+  |
//  | |    |   n0   |    |       |     |   n1   |    |  |
//  | +----|--------|----+       +-----|--------|----+  |
//  | |    csmaDevice    |       |     csmaDevice    |  |--------------------------
//  | +------------------+       +-------------------+  |Network:
//  |        ||.1                          ||.161       |10.1.3.0/24
//  |        ||            +--+            ||           |
//  |        ||         .34|n3|.65         ||           |Sub Nets:
//  |        ||            +--+            ||           |10.1.3.0/27
//  |        ||       .33//    \\ .66      ||           |10.1.3.32/27
//  |        ||   .2+--+          +--+.162 ||           |10.1.3.64/27
//  |        |======|n2|          |n4|======|           |10.1.3.96/27
//  |               +--+          +--+                  |10.1.3.128/27
//  |               .129\\      //.97                   |10.1.3.160/27
//  |                  .130+--+.98                      |10.1.3.192/27 (spare)
//  |                      |n5|                         |10.1.3.224/27 (spare)
//  |                      +--+                         |--------------------------
//  |             Simulated Mesh Network                |
//  |                                                   |
//  |                   ns3-simulation                  |
//  +---------------------------------------------------+
//  |                   Host computer                   |
//  +---------------------------------------------------+
//
// My CPU layout from dpdk's usertools/cpu_layout.py:
/*
======================================================================
Core and Socket Information (as reported by '/sys/devices/system/cpu')
======================================================================

cores =  [0, 1, 2, 3, 4, 5]
sockets =  [0]

       Socket 0       
       --------       
Core 0 [0, 6]         
Core 1 [1, 7]         
Core 2 [2, 8]         
Core 3 [3, 9]         
Core 4 [4, 10]        
Core 5 [5, 11]
*/

#include "ns3/abort.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/fd-net-device-module.h"
#include "ns3/internet-apps-module.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/applications-module.h"
#include "ns3/traffic-control-module.h"
#include "ns3/csma-module.h"
#include "ns3/bridge-module.h"
#include "ns3/point-to-point-module.h"


using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("DpdkSwitchTopo");


int
main (int argc, char *argv[])
{
    LogComponentEnable ("DpdkSwitchTopo", LOG_INFO);
    
    //Set Default Realtime simulator to Hard Limit (1) instead of Best Effort (0). Default value is 0.1 seconds off from realtime and the program will terminate.
    //Config::SetDefault ("ns3::RealtimeSimulatorImpl::SynchronizationMode", EnumValue (1));
    //Uncomment line below to set other offsets from real clock scheduler to something other than 0.1 seconds
    //Config::SetDefault ("ns3::RealtimeSimulatorImpl::HardLimit", TimeValue (MicroSeconds (1)));
    NS_LOG_DEBUG ("Setting real time simulation");
    GlobalValue::Bind ("SimulatorImplementationType", StringValue ("ns3::RealtimeSimulatorImpl"));
    GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));

    //Setting default routing values
    //Config::SetDefault ("ns3::Ipv4GlobalRouting::RandomEcmpRouting", BooleanValue (true));
    //Config::SetDefault ("ns3::Ipv4GlobalRouting::RespondToInterfaceEvents", BooleanValue (true));
    
    std::string devName[2] = { "0000:05:00.0", "0000:05:00.3" };// Device enp5s0f0 and enp5s0f3 pci number. "lspci" to find
    //std::string dpdkDriver ("uio_pci_generic"); //generic driver
    //Virtual Function I/O (vfio) driver. IOMMU/device agnostic framework for exposing direct device access to userspace,in a secure, IOMMU protected environment
    //See https://www.kernel.org/doc/Documentation/vfio.txt
    std::string dpdkDriver ("vfio-pci"); //vfio-pci driver
    std::string pmdLibrary ("librte_net_e1000.so"); //I350 NIC should use librte_net_e1000 according to http://doc.dpdk.org/guides/nics/igb.html. Should also work for integrated NIC I219-LM
    std::string memLibrary ("librte_mempool_ring.so");
    std::string lCorelist ("4,5,10,11"); //set what logical cores you want to use in the simulation (int). See cpu cores with "lscpu | grep CPU" or "lstopo"

    std::string pcapDpdk ("dpdk-physPort-Pcap");
    std::string pcapCsma1 ("dpdk-csma1-Pcap");
    std::string pcapCsma4 ("dpdk-csma4-Pcap");
    std::string routesOut ("dpdk-MeshTopo-routes.routes");

    //Create nodes
    NS_LOG_DEBUG ("Creating nodes");
    NodeContainer dpdkNodes;
    dpdkNodes.Create(2);
    NodeContainer meshNodes;
    meshNodes.Create(4);

    //Create a Dpdk NetDevice using Dpdk NetDeviceHelper
    NS_LOG_DEBUG ("Creating DpdkNetDevice");
    DpdkNetDeviceHelper dpdk;

    //Setting MaxTx burst to 1 to give lower latency and no packet batching
    dpdk.SetAttribute ("MaxTxBurst", UintegerValue (1));

    //Setting parameters for first dpdk device
    NS_LOG_DEBUG ("Setting Parameters for dpdk device");
    NS_LOG_DEBUG ("DeviceName: " << devName[0] << "\nDPDK driver set to: " << dpdkDriver << "\npmdLibrary set to: " << pmdLibrary << "\nlCorelist set to: " << lCorelist);
    dpdk.SetDeviceName (devName[0]);
    dpdk.SetDpdkDriver (dpdkDriver);
    dpdk.SetPmdLibrary (pmdLibrary);
    dpdk.SetMemLibrary (memLibrary);
    dpdk.SetLCoreList (lCorelist);
    dpdk.InitDpdkEal(2, devName);
    dpdk.SetLCore(5); //Set what core number this device shall use. It must be listed in the lCorelist otherwise the simulation will stop.

    //Install first dpdk device into node 0 and add it to the device container. This will start the dpdk device.
    NS_LOG_DEBUG ("Installing DpdkNetDevice into node 0 and starting device");
    NetDeviceContainer devices = dpdk.Install (dpdkNodes.Get(0));

    NS_LOG_DEBUG("Setting devicename for next device to: " << devName[1]);
    dpdk.SetDeviceName (devName[1]);
    dpdk.SetLCore(11); //Set what core number this device shall use. It must be listed in the lCorelist otherwise the simulation will stop.

    //Install dpdk device into node 1. Also add the device into the devices container
    NS_LOG_DEBUG ("Installing DpdkNetDevice into node 1 and starting device");
    devices.Add(dpdk.Install (dpdkNodes.Get(1)));
    
    NS_LOG_DEBUG ("Setting MAC address to node 0's device");
    Ptr<NetDevice> device0 = devices.Get (0);
    NS_LOG_DEBUG ("Setting MAC address to node 1's device");
    Ptr<NetDevice> device1 = devices.Get (1);

    //Get all nodes and install the IP-stack 
    NS_LOG_DEBUG ("Installing IP-stack to all nodes");
    InternetStackHelper stack;
    stack.Install (NodeContainer::GetGlobal ());


    NS_LOG_DEBUG ("Installing csma devices with channel to all nodes");
    //PointToPointHelper csma0, csma1, csma2, csma3, csma4, csma5; //PtpDevice give RS-232/422 behaviour https://www.nsnam.org/docs/models/html/point-to-point.html
    CsmaHelper csma0, csma1, csma2, csma3, csma4, csma5; //Routing module buggy, Won't calculate equal path with csma https://www.nsnam.org/bugzilla/show_bug.cgi?id=667
    NetDeviceContainer csma0Devices, csma1Devices, csma2Devices, csma3Devices, csma4Devices, csma5Devices;
    csma0Devices = csma0.Install(NodeContainer (dpdkNodes.Get(0), meshNodes.Get(0)));
    csma1Devices = csma1.Install(NodeContainer (meshNodes.Get(0), meshNodes.Get(1)));
    csma2Devices = csma2.Install(NodeContainer (meshNodes.Get(1), meshNodes.Get(2)));
    csma3Devices = csma3.Install(NodeContainer (meshNodes.Get(2), meshNodes.Get(3)));
    csma4Devices = csma4.Install(NodeContainer (meshNodes.Get(0), meshNodes.Get(3)));
    csma5Devices = csma5.Install(NodeContainer (dpdkNodes.Get(1), meshNodes.Get(2)));

    //Uncomment if PointToPoint is used
    /*
    csma0.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
    csma1.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
    csma2.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
    csma3.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
    csma4.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
    csma5.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
    */

    NS_LOG_DEBUG ("Defining IP-networks");
    Ipv4AddressHelper addressDpdk0, addressDpdk1, addressMesh0, addressMesh1, addressMesh2, addressMesh3, addressMesh4, addressMesh5;
    addressDpdk0.SetBase ("10.1.1.0", "255.255.255.0");
    addressDpdk1.SetBase ("10.1.2.0", "255.255.255.0");
    addressMesh0.SetBase ("10.1.3.0", "255.255.255.224");
    addressMesh1.SetBase ("10.1.3.32", "255.255.255.224");
    addressMesh2.SetBase ("10.1.3.64", "255.255.255.224");
    addressMesh3.SetBase ("10.1.3.96", "255.255.255.224");
    addressMesh4.SetBase ("10.1.3.128", "255.255.255.224");
    addressMesh5.SetBase ("10.1.3.160", "255.255.255.224");

    //The spare sub-nets from 10.1.3.0/24 if needed later
    //addressMesh6.SetBase ("10.1.3.192", "255.255.255.224");
    //addressMesh7.SetBase ("10.1.3.224", "255.255.255.224");

    //Setting IP-addresses to all the interfaces
    NS_LOG_DEBUG ("Assigning IP-addresses to al interfaces");
    Ipv4InterfaceContainer interface0 = addressDpdk0.Assign (device0);
    Ipv4InterfaceContainer interface1 = addressDpdk1.Assign (device1);
    Ipv4InterfaceContainer interface2 = addressMesh0.Assign (csma0Devices);
    Ipv4InterfaceContainer interface3 = addressMesh1.Assign (csma1Devices);
    Ipv4InterfaceContainer interface4 = addressMesh2.Assign (csma2Devices);
    Ipv4InterfaceContainer interface5 = addressMesh3.Assign (csma3Devices);
    Ipv4InterfaceContainer interface6 = addressMesh4.Assign (csma4Devices);
    Ipv4InterfaceContainer interface7 = addressMesh5.Assign (csma5Devices);
    
    NS_LOG_DEBUG ("Setting up static routes between 10.1.1.0/24 and 10.1.2.0/24");
    //Setting Static routes because CSMA channels mess up OSPF
    Ipv4StaticRoutingHelper stat;

    //Default setting static route through nodes 4-5-2 to 10.1.1.0/24 and 2-3-4 to 10.1.2.0/24
    //Routing Table Node 0
    Ptr<Ipv4StaticRouting> staticRoutingNode0 = stat.GetStaticRouting (interface0.Get(0).first);
    staticRoutingNode0->AddNetworkRouteTo(Ipv4Address("10.1.2.0"), Ipv4Mask("255.255.255.0"), Ipv4Address("10.1.3.2"), interface2.Get(0).second, 0);  

    //Routing table Node 1
    Ptr<Ipv4StaticRouting> staticRoutingNode1 = stat.GetStaticRouting (interface1.Get(0).first);
    staticRoutingNode1->AddNetworkRouteTo(Ipv4Address("10.1.1.0"), Ipv4Mask("255.255.255.0"), Ipv4Address("10.1.3.162"), interface7.Get(0).second, 0);

    //Routing Table Node 2
    Ptr<Ipv4StaticRouting> staticRoutingNode2Fr0 = stat.GetStaticRouting (interface2.Get(1).first);
    staticRoutingNode2Fr0->AddNetworkRouteTo(Ipv4Address("10.1.2.0"), Ipv4Mask("255.255.255.0"), Ipv4Address("10.1.3.34"), interface3.Get(0).second, 0);

    //Uncomment here and comment above to send through nodes 2-5-4
    //Ptr<Ipv4StaticRouting> staticRoutingNode2Fr0 = stat.GetStaticRouting (interface2.Get(1).first);
    //staticRoutingNode2Fr0->AddNetworkRouteTo(Ipv4Address("10.1.2.0"), Ipv4Mask("255.255.255.0"), Ipv4Address("10.1.3.130"), interface6.Get(0).second, 0);    

    Ptr<Ipv4StaticRouting> staticRoutingNode2Fr5 = stat.GetStaticRouting (interface6.Get(0).first);
    staticRoutingNode2Fr5->AddNetworkRouteTo(Ipv4Address("10.1.1.0"), Ipv4Mask("255.255.255.0"), Ipv4Address("10.1.3.1"), interface2.Get(1).second, 0); 

    //Routing Table Node 3
    Ptr<Ipv4StaticRouting> staticRoutingNode3 = stat.GetStaticRouting (interface4.Get(0).first);
    staticRoutingNode3->AddNetworkRouteTo(Ipv4Address("10.1.1.0"), Ipv4Mask("255.255.255.0"), Ipv4Address("10.1.3.33"), interface3.Get(1).second, 0);  

    Ptr<Ipv4StaticRouting> staticRoutingNode3Back = stat.GetStaticRouting (interface3.Get(1).first);
    staticRoutingNode3Back->AddNetworkRouteTo(Ipv4Address("10.1.2.0"), Ipv4Mask("255.255.255.0"), Ipv4Address("10.1.3.66"), interface4.Get(0).second, 0); 

    //Routing table Node 4
    Ptr<Ipv4StaticRouting> staticRoutingNode4 = stat.GetStaticRouting (interface7.Get(1).first);
    staticRoutingNode4->AddNetworkRouteTo(Ipv4Address("10.1.1.0"), Ipv4Mask("255.255.255.0"), Ipv4Address("10.1.3.98"), interface5.Get(0).second, 0);
    //Uncomment this and comment above to send 4-3-2 instead
    //Ptr<Ipv4StaticRouting> staticRoutingNode4 = stat.GetStaticRouting (interface7.Get(1).first);
    //staticRoutingNode4->AddNetworkRouteTo(Ipv4Address("10.1.1.0"), Ipv4Mask("255.255.255.0"), Ipv4Address("10.1.3.65"), interface4.Get(1).second, 0);    

    Ptr<Ipv4StaticRouting> staticRoutingNode4Fr3 = stat.GetStaticRouting (interface4.Get(1).first);
    staticRoutingNode4Fr3->AddNetworkRouteTo(Ipv4Address("10.1.2.0"), Ipv4Mask("255.255.255.0"), Ipv4Address("10.1.3.161"), interface7.Get(1).second, 0); 

    //Routing Table Node 5
    Ptr<Ipv4StaticRouting> staticRoutingNode5Fr2 = stat.GetStaticRouting (interface6.Get(1).first);
    staticRoutingNode5Fr2->AddNetworkRouteTo(Ipv4Address("10.1.2.0"), Ipv4Mask("255.255.255.0"), Ipv4Address("10.1.3.97"), interface5.Get(1).second, 0); 
    
    Ptr<Ipv4StaticRouting> staticRoutingNode5Fr4 = stat.GetStaticRouting (interface5.Get(1).first);
    staticRoutingNode5Fr4->AddNetworkRouteTo(Ipv4Address("10.1.1.0"), Ipv4Mask("255.255.255.0"), Ipv4Address("10.1.3.129"), interface6.Get(1).second, 0);  
    
 
    //If ptp channels are used we can use automatic routing algorithms based on LSA (OSPF) instead of static routes https://www.nsnam.org/docs/models/html/routing-overview.html
    //Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

    NS_LOG_INFO("Writing pcap files from dpdk devices to prefix: " << pcapDpdk << "\nWriting pcap files from csma devices to prefix: " << pcapCsma1 << " and " << pcapCsma4);
    dpdk.EnablePcap (pcapDpdk, devices, true);
    csma1.EnablePcap (pcapCsma1, csma1Devices.Get(0), true);
    csma4.EnablePcap (pcapCsma4, csma4Devices.Get(0), true);
    Ipv4GlobalRoutingHelper g;
    NS_LOG_INFO("Writing routing tables to prefix: " << routesOut);
    Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> (routesOut, std::ios::out);
    g.PrintRoutingTableAllAt (Seconds (5), routingStream);
    g.PrintNeighborCacheAllAt (Seconds (5), routingStream);

    NS_LOG_INFO ("Running the actual simulation");
    //Simulator::Schedule (Seconds (5),&Ipv4GlobalRoutingHelper::RecomputeRoutingTables);
    Simulator::Stop (Seconds (10));
    Simulator::Run ();
    Simulator::Destroy ();
}